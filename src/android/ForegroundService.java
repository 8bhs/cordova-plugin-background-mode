/*
 Copyright 2013 Sebasti�n Katzer

 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 */

package de.appplant.cordova.plugin.background;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.*;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Icon;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import org.json.JSONObject;
import androidx.core.app.NotificationCompat;

import android.util.Log;

import static android.app.PendingIntent.FLAG_IMMUTABLE;
import static android.os.PowerManager.PARTIAL_WAKE_LOCK;

/**
 * Puts the service in a foreground state, where the system considers it to be
 * something the user is actively aware of and thus not a candidate for killing
 * when low on memory.
 */
public class ForegroundService extends Service {
    private static final String TAG = "ForegroundService";

    // Fixed group name for the 'foreground' notification
    private static final String NOTIFICATION_GROUP = "Cordova.Foreground.Service.Not.Group";

    // Fixed ID for the 'foreground' notification
    public static final int NOTIFICATION_ID = -574543954;

    // Default title of the background notification
    private static final String NOTIFICATION_TITLE =
            "Oops, something when wrong";

    // Default text of the background notification
    private static final String NOTIFICATION_TEXT =
            "An internal process has been stopped, please restart the app. If error persists, please contact support for assistance.";

    // Default color of the background notification
    private static final String NOTIFICATION_COLOR = "f04141";

    // Default icon of the background notification
    private static final String NOTIFICATION_ICON = "notification";

    // Binder given to clients
    private final IBinder binder = new ForegroundBinder();

    // Flag used to determine if BackgroundMode has been bound to ForegroundService
    // Properly instantiated ForegroundService will always be bound before startService
    private boolean isBoundToApp = false;

    // Partial wake lock to prevent the app from going to sleep when locked
    private PowerManager.WakeLock wakeLock;

    /**
     * Allow clients to call on to the service.
     */
    @Override
    public IBinder onBind (Intent intent) {
        Log.d(TAG, "onBind");

        isBoundToApp = true;

        return binder;
    }

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    class ForegroundBinder extends Binder
    {
        ForegroundService getService()
        {
            // Return this instance of ForegroundService
            // so clients can call public methods
            return ForegroundService.this;
        }
    }

    /**
     * Put the service in a foreground state to prevent app from being killed
     * by the OS.
     */
    @Override
    public void onCreate()
    {
        Log.d(TAG, "onCreate");

        super.onCreate();
        keepAwake();
    }

    /**
     * No need to run headless on destroy.
     */
    @Override
    public void onDestroy()
    {
        Log.d(TAG, "onDestroy");

        super.onDestroy();
        sleepWell();
    }

    /**
     * Prevent Android from stopping the background service automatically.
     */
    @Override
    public int onStartCommand (Intent intent, int flags, int startId) {
        Log.d(TAG, "onStart");

        ensureAppRunning();

        return START_STICKY;
    }

    private void ensureAppRunning()
    {
        Log.d(TAG, "ensureAppRunning");

        if(isRestartedService())
        {
            Log.d(TAG, "ensureAppRunning: starting app");
            startApp();
        }
    }

    private boolean isRestartedService() {
        Log.d(TAG, "isRestartedService: is bound to app - " + isBoundToApp);

        // If service is not bound yet, then app has died and this service instance was restarted without MainActivity.
        return !isBoundToApp;
    }

    private void startApp() {
        this.broadcastEvent(-1);
    }

    private void broadcastEvent(int pid) {
        Context context = getApplicationContext();
        String packageName = context.getPackageName();
        String actionFilter = packageName + ".action.startApp";

        Intent i = new Intent();
        i.setPackage(packageName);  // Required for BroadcastReceiver to work
        i.setAction(actionFilter);
        i.putExtra("pid", pid);

        Log.d(TAG, "broadcastEvent: sending action - " + actionFilter);
        context.sendBroadcast(i);
    }

    /**
     * Put the service in a foreground state to prevent app from being killed
     * by the OS.
     */
    @SuppressLint("WakelockTimeout")
    private void keepAwake()
    {
        JSONObject settings = BackgroundMode.getSettings();
        boolean isSilent    = settings.optBoolean("silent", false);

        if (!isSilent) {
            startForeground(NOTIFICATION_ID, makeNotification());
        }

        PowerManager pm = (PowerManager)getSystemService(POWER_SERVICE);

        wakeLock = pm.newWakeLock(
                PARTIAL_WAKE_LOCK, "backgroundmode:wakelock");

        wakeLock.acquire();
    }

    /**
     * Stop background mode.
     */
    private void sleepWell()
    {
        stopForeground(true);
        getNotificationManager().cancel(NOTIFICATION_ID);

        if (wakeLock != null) {
            wakeLock.release();
            wakeLock = null;
        }
    }

    /**
     * Create a notification as the visible part to be able to put the service
     * in a foreground state by using the default settings.
     */
    private Notification makeNotification()
    {
        return makeNotification(BackgroundMode.getSettings());
    }

    /**
     * Create a notification as the visible part to be able to put the service
     * in a foreground state.
     *
     * @param settings The config settings
     */
    private Notification makeNotification (JSONObject settings)
    {
        // use channelid for Oreo and higher
        String CHANNEL_ID = "cordova-plugin-background-mode-id";
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // The user-visible name of the channel.
            CharSequence name = settings.optString("channelName", "Background Mode");
            // The user-visible description of the channel.
            String description = settings.optString("channelDescription", "Background Mode Notification");

            int importance = NotificationManager.IMPORTANCE_LOW;

            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);

            // Configure the notification channel.
            mChannel.setDescription(description);

            getNotificationManager().createNotificationChannel(mChannel);
        }
        String title    = settings.optString("title", NOTIFICATION_TITLE);
        String text     = settings.optString("text", NOTIFICATION_TEXT);
        boolean bigText = settings.optBoolean("bigText", false);
        String subText = settings.optString("subText", "");
        String visibility = settings.optString("visibility", "");

        Context context = getApplicationContext();
        String pkgName  = context.getPackageName();
        Intent intent   = context.getPackageManager()
                .getLaunchIntentForPackage(pkgName);

        int smallIcon = getIconResId(settings);
        if (smallIcon == 0) { //If no icon at all was found, fall back to the app's icon
            smallIcon = context.getApplicationInfo().icon;
        }

        NotificationCompat.Builder notification = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setContentTitle(title)
                .setContentText(text)
                .setOngoing(true)
                .setSmallIcon(smallIcon)
                .setShowWhen(settings.optBoolean("showWhen", true));

        if(settings.optBoolean("ownGroup", true)) {
            notification = notification.setGroup(NOTIFICATION_GROUP);
        }

        if (!subText.equals("")) {
            notification.setSubText(subText);
        }

        if (settings.optBoolean("allowClose", false)) {

            final Intent clostAppIntent = new Intent("com.backgroundmode.close" + pkgName);
            final PendingIntent closeIntent = PendingIntent.getBroadcast(context, 1337, clostAppIntent, immutableFlag(0));
            final String closeIconName = settings.optString("closeIcon", "power");
            NotificationCompat.Action.Builder closeAction = new NotificationCompat.Action.Builder(getIconResId(closeIconName), settings.optString("closeTitle", "Close"), closeIntent);
            notification.addAction(closeAction.build());
        }

        if (settings.optBoolean("hidden", true)) {
            notification.setPriority(Notification.PRIORITY_MIN);
        }

        if (bigText || text.contains("\n")) {
            notification.setStyle(
                    new NotificationCompat.BigTextStyle().bigText(text));
        }

        if (!visibility.equals("")) {
            notification.setVisibility(getVisibility(visibility));
        }

        setColor(notification, settings);

        if (intent != null && settings.optBoolean("resume")) {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent contentIntent = PendingIntent.getActivity(
                    context, NOTIFICATION_ID, intent,
                    immutableFlag(PendingIntent.FLAG_UPDATE_CURRENT));


            notification.setContentIntent(contentIntent);
        }

        return notification.build();
    }

    /**
     * Update the notification.
     *
     * @param settings The config settings
     */
    protected void updateNotification (JSONObject settings)
    {
        boolean isSilent = settings.optBoolean("silent", false);

        if (isSilent) {
            stopForeground(true);
            return;
        }

        Notification notification = makeNotification(settings);
        getNotificationManager().notify(NOTIFICATION_ID, notification);

    }

    /**
     * Retrieves the resource ID of the sent icon name
     *
     * @param name Name of the resource to return
     */
    private int getIconResId(String name) {
        int resId = getIconResId(name, "mipmap");

        if (resId == 0) {
            resId = getIconResId(name, "drawable");
        }

        if (resId == 0) {
            resId = getIconResId("icon", "mipmap");
        }

        if (resId == 0) {
            resId = getIconResId("icon", "drawable");
        }


        return resId;
    }

    /**
     * Retrieves the resource ID of the app icon.
     *
     * @param settings A JSON dict containing the icon name.
     */
    private int getIconResId(JSONObject settings) {
        String icon = settings.optString("icon", NOTIFICATION_ICON);

        return getIconResId(icon);
    }

    /**
     * Retrieve resource id of the specified icon.
     *
     * @param icon The name of the icon.
     * @param type The resource type where to look for.
     *
     * @return The resource id or 0 if not found.
     */
    private int getIconResId (String icon, String type)
    {
        Resources res  = getResources();
        String pkgName = getPackageName();

        return res.getIdentifier(icon, type, pkgName);
    }

    /**
     * Get the visibility constant from a string.
     *
     * @param visibility one of 'public', 'private', 'secret'
     *
     * @return The visibility constant if a match is found, 'private' otherwise
     */
    private int getVisibility (String visibility)
    {
        if (visibility.equals("public")) {
            return Notification.VISIBILITY_PUBLIC;
        } else if (visibility.equals("secret")) {
            return Notification.VISIBILITY_SECRET;
        } else {
            return Notification.VISIBILITY_PRIVATE;
        }
    }
    /**
     * Set notification color if its supported by the SDK.
     *
     * @param notification A Notification.Builder instance
     * @param settings A JSON dict containing the color definition (red: FF0000)
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void setColor(NotificationCompat.Builder notification,
                          JSONObject settings) {

        String hex = settings.optString("color", NOTIFICATION_COLOR);

        Log.d(TAG, "setColor: color - " + hex);

        if (Build.VERSION.SDK_INT < 21 || hex == null)
            return;

        try {
            int aRGB = Integer.parseInt(hex, 16) + 0xFF000000;
            notification.setColor(aRGB);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the shared notification service manager.
     */
    private NotificationManager getNotificationManager()
    {
        return (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    }

    private int immutableFlag(int flags) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return flags | FLAG_IMMUTABLE;
        }
        else {
            return flags;
        }
    }
}